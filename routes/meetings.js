var express = require('express');
var router = express.Router();
var http = require("http");
// Handle get action and show login form
router.all('*', function(req, res, next) {
    console.log('--in meetings check --');
    if (!req.session.isLogin) {
        res.json({
            login: false
        });
    } else {
        next();
    }
});
/**
 * add new booking
 * Set a meeting
    POST http://,meetingserver.com/calendarId
    JSON data: {“CalendarId”: calendarId, “StartTime”: time, “Duration”:
    durationInMinutes, “StartDate”: date, “subject”: subject_description}
    Response: MeetingId or fail
 */
router.post('/add', function(req, res, next) {
    // call third party api to add a booking
    var clenderid = req.body.clenderid || false;
    if (!!clenderid) {
        var dataToPost = querystring.stringify({
            'CalendarId': clenderid,
            'StartTime': req.body.startTime,
            'Duration': req.body.durationInMinutes,
            'StartDate': req.body.StartDate,
            'subject': req.body.subject,
        });
        var options = {
            hostname: 'meetingserver.com',
            port: 80,
            path: '/' + clenderid,
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': dataToPost.length
            }
        };
        var post = http.request(options, function(result) {
            console.log("statusCode: ", result.statusCode);
            var meetingId = '';
            result.on('data', function(d) {
                meetingId = d;
            });
            result.on('end', function() {
                res.json({
                    success: true,
                    'error': false,
                    meetingId: meetingId
                });
            });
        });
        // Handle any error ocuured in api request 
        post.on('error', function(e) {
            console.error(e);
            res.send({
                success: false,
                'error': e
            })
        });
        // write data to request body
        post.write(dataToPost);
        post.end();
    } else {
        res.json({
            success: false,
            'error': 'Empty calendarId'
        })
    }
});
// cancle a meeting
// DELETE http://,meetingserver.com/calendarId/meetingId
// Response: Ok or fail
router.post('/cancle', function(req, res, next) {
    // call third party api to add a booking
    var clenderid = req.body.clenderid || false;
    var meetingId = req.body.meetingid || false;
    if (!!clenderid && !!meetingId) {
        var options = {
            hostname: 'meetingserver.com',
            port: 80,
            path: '/' + clenderid + '/' + meetingId,
            method: 'DELETE',
        };
        var post = http.request(options, function(result) {
            console.log("statusCode: ", result.statusCode);
            var response = '';
            result.on('data', function(d) {
                response = d;
            });
            result.on('end', function() {
                res.json({
                    success: response == 'Ok',
                    'error': false,
                });
            });
        });
        // Handle any error ocuured in api request 
        post.on('error', function(e) {
            console.error(e);
            res.send({
                success: false,
                'error': e
            })
        });
        // write data to request body
        post.write(dataToPost);
        post.end();
    } else {
        res.json({
            success: false,
            'error': 'Empty calendarId or meetingId'
        })
    }
});
// get booking listing
router.get('/all', function(req, res) {
    // Get all booking listing from DB
});
// just to handle /meetings/ page
router.get('/', function(req, res, next) {
    if (!!req.session.isLogin) {
        var isAdmin = !!req.session.isAdminLogin ? ' for Admin' : '';
        res.send('<a href="/users/logout">Logout' + isAdmin + '</a>');
    } else {
        res.send('<a href="/users/login">Login</a>');
    }
});
module.exports = router;