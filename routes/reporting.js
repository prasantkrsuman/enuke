/**
 *  5. Admin users can clear all the system counters from the reporting subsystem
 *  6. Admin users can request to see all the counters managed by the reporting subsystem
 *  7. Admin users can request a list of users that are currently logged in and for each user, the time he has been logged in is also reported
 *  9. The Admin user can start/stop the real time reports when he wants via an API call.
 */
var express = require('express');
var router = express.Router();
// Handle get action and show login form
router.all('*', function(req, res, next) {
    console.log('--in reporting check --');
    if (!req.session.isAdminLogin) {
        res.json({
            login: false,
            isAdmin: false
        });
    } else {
        next();
    }
});
// Handle post action 
router.post('/clear-counters', function(req, res, next) {
    // Do the db operation to clear all system counters if done succesfully response true else false 
    var isDone = true;
    res.json({
        isDone: isDone
    });
});
// Handle realtime start/stop
router.post('/realtime', function(req, res, next) {
    // For realtime data we need to use socket.io to emit messages from server to client side browser
    // By this api we will manage if admin wants realtime data or not
    // we will set a flag and this flag will be tested while emiting data from socket.
    var isRealtimeOn = req.body.realtime || 'start';
    // Update db with flag 
    var isDone = true;
    res.json({
        isDone: isDone
    });
});
// see all counters managed
// The following counters are managed by the reporting subsystem:
// 1. Pending requests: Meetings or login/logout requests that are currently in process of been served
// 2. Average request time for all requests since the counters were cleared
// 3. Number of meetings scheduled since the counters were cleared
// 4. Number of meetings cancelled since the counter were cleared
// 5. Total number of requests processed (Login/Logout, Set/Cancel meetings) since the counters were cleared
router.get('/counters', function(req, res) {
    // some db interaction here and then response with array of counters object
    var counterType = req.body.type || false;
    if (counterType) {
        switch (counterType) {
            case 'pending-request':
                res.json({
                    data: [{}], // list of pending request
                    error: false
                });
                break;
            // Manage other cases
            case '':
                // code block
                break;
        }
    } else {
        res.json({
            data: [],
            error: "Type empty"
        });
    }
});
// see a list of users currently logined
router.get('/users-logined', function(req, res) {
    // when a user will login update isLogged : true and loggedIn_timestamp : new Date.Now()
    // some db interaction here where query is {isLogged : true} and then response with array of users object
    res.json({
        data: [{
            uid: 1,
            name: 'user1',
            logintime: 1456604734
        }, {
            uid: 2,
            name: 'user2',
            logintime: 1456604234
        }],
        error: false
    });
});
// just to handle /reporting page and show html
router.get('/', function(req, res, next) {
    // console.log(req.session);
    if (!!req.session.isLogin) {
        var isAdmin = !!req.session.isAdminLogin ? ' for Admin' : '';
        res.send('<a href="/users/logout">Logout' + isAdmin + '</a>');
    } else {
        res.send('<a href="/users/login">Login</a>');
    }
});
module.exports = router;