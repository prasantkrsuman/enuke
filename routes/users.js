var express = require('express');
var router = express.Router();
// Handle get action and show login form
router.get('/login', function(req, res, next) {
    res.render('users/index');
});
// Handle post action 
router.post('/login', function(req, res, next) {
    var username = req.body.username || false;
    var password = req.body.password || false;
    console.log('username => ' + username + ' password => ' + password);
    if (username && password) {
        req.session.username = username;
        req.session.isLogin = true;
        if (username == 'admin') {
            req.session.isAdminLogin = true;
        }
        res.json({
            login: true
        });
    } else {
        res.json({
            login: false
        });
    }
});
// logout action
router.all('/logout', function(req, res) {
    req.session.destroy(function(err) {
        if (err) {
            console.log(err);
            res.json({
                logout: false
            });
        } else {
            res.json({
                logout: true
            });
        }
    });
});
//  just to handle /users/ page show html 
router.get('/', function(req, res, next) {
    if (!!req.session.isLogin) {
        var isAdmin = !!req.session.isAdminLogin ? ' for Admin' : '';
        res.send('<a href="/users/logout">Logout' + isAdmin + '</a>');
    } else {
        res.send('<a href="/users/login">Login</a>');
    }
});
module.exports = router;